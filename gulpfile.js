var gulp = require('gulp');
var plugins = require('gulp-load-plugins')();
var autoprefixer = require('gulp-autoprefixer');
var sass = require('gulp-sass');
var modRewrite = require('connect-modrewrite');
var browserSync = require('browser-sync');

// Autoprefixer options
var autoprefixerOptions = {
    browsers: [
        'Chrome >= 38',
        'Firefox >= 38',
        'Opera >= 28',
        'Edge >= 12',
        'IE >= 10'
    ]
};

gulp.task('sass', function () {
    return gulp.src(['./resources/scss/*.scss', './resources/scss/**/*.scss'])
        .pipe(sass({
            outputStyle: 'compressed'
        }).on('error', sass.logError))
        .pipe(plugins.autoprefixer(autoprefixerOptions))
        .pipe(gulp.dest('./resources/css'));
});

// launch browsersynch
gulp.task('watch', ['browserSyncInit', 'sass'], function () {
    // sass changes
    gulp.watch(['resources/scss/*.scss', 'resources/scss/**/*.scss'], ['sass', browserSync.reload]);
    // html changes
    gulp.watch(['*.html', 'templates/*.html'], browserSync.reload);
});

// browserSync init task
gulp.task('browserSyncInit', function () {
    browserSync.init({
        server: {
            directory: true,
            baseDir: './',
        },
        open: false
    });
});
