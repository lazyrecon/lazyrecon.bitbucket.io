jQuery.noConflict();
(function ($) {
    $(function () {
        window.loadUI();
        $('#howManyHour').SumoSelect({
            'placeholder': $('#howManyHour').data('placeholder')
        })
        $('#whichMachines').SumoSelect({
            'placeholder': $('#whichMachines').data('placeholder')
        })
    });
    window.loadUI = function () {
        $('.hide').hide();
        $('.has-more-content').click(function () {
            if ($(this).is('.open')) {
                $(this).find(".hide").hide(500);
            } else {
                $(this).find(".hide").show(500);
            }
            $(this).toggleClass('open')
            console.log("SI");
        })
    }
})(jQuery);


