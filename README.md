# Realtime dashboard

### Docker

`/laradock`

##### Use workspace container

-   Start **Workspace** container

        docker-compose up -d workspace

-   Log in container as **laradock** user

        docker-compose exec --user=laradock workspace bash

-   Use Docker bash

        docker-compose exec --user=laradock workspace bash

### NPM

`/public`

    npm install

### DEV

`/public`

    gulp watch

BrowserSync at <http://172.19.0.2:3000/>

##### Sass

###### Mobile First

In order to rite mobile first sass define media query with _min-\*_ definition

    @media all and (min-width:$bp-s) {...}

###### Breakpoints

`/scss/vars`:

-   `$bp-s`: 678px;
-   `$bp-m`: 768px;
-   `$bp-l`: 1500px;
-   `$bp-xl`: 1920px;

###### Use BEM convention

Define **Block** component

    .selector

Define component variation

    .selector--variation

Define component child element

    .selector__child

### Machine & Job status

##### Machine status

-   alarm
-   auto
-   maintainance
-   manual
-   notready
-   off
-   production
-   ready
-   wait

###### Job status

-   job_aborted
-   job_completed
-   job_created
-   job_ended
-   job_incomplete
-   job_production
-   job_saved
-   job_suspended
-   job_transferred
-   job_undefined
-   job_working
